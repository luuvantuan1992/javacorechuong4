package lesson2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class MyFatorial {

	public static void main(String[] args) {
		System.out.println("Nhập vào số cần tính giai thừa...");
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		try {
			int value = Integer.parseInt(br.readLine());
			System.out.println("Giai thừa của " + value + " là "
					+ MyFatorial.getFatorial(value));
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static long getFatorial(int value) {
		long result = 1;
		do {
			result = result * value;
			--value;
		} while (value > 1);
		return result;
	}

}

