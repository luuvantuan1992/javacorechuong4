package Example3;

import java.util.*;

public class StringTokenizerImplementer {
	public static void main(String args[]) {
		// đặt một biểu thức toán học trong một chuỗi và tạo một tokenizer cho chuỗi đó.
		String mathExpr = "4*3+2/4";
		StringTokenizer st1 = new StringTokenizer(mathExpr, "*+/-", true);
//trong khi có các token trái, hãy hiển thị mỗi token 
		System.out.println("Tokens f mathExpr: ");
		while (st1.hasMoreTokens())
			System.out.println(st1.nextToken());
		// tạo một chuỗi của các trường được phân cách bởi dấu phẩy và tạo một tokenizer
		// cho chuỗi.
		String commas = "field1,field2,field3,and field4";
		StringTokenizer st2 = new StringTokenizer(commas, ",", false);
		// trong khi có các token trái, hãy hiển thị mỗi token.
		System.out.println("Comma-delimited tokens : ");
		while (st2.hasMoreTokens())
			System.out.println(st2.nextToken());
	}
}
