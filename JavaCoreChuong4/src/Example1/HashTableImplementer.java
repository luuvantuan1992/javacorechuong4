package Example1;

import java.util.*;

public class HashTableImplementer {
	public static void main(String args[]) {
		// tạo một bảng băm mới
		Hashtable ht = new Hashtable();
		// thêm các tập ảnh tốt nhất của Pink Floyd
		ht.put("Pulse", new Integer(1995));
		ht.put("Dark Side of the Moon", new Integer(1973));
		ht.put("Wish You Were Here", new Integer(1975));
		ht.put("Animals", new Integer(1997));
		ht.put("Ummagumma", new Integer(1969));
		// Hiển thị bảng băm
		System.out.println("Initailly: " + ht.toString());
		// kiểm tra cho bất kỳ tập ảnh nào từ 1969
		if (ht.contains(new Integer(1969)))
			System.out.println("An album from 1969 exists");
		// kiểm tra cho tập ảnh các con thú
		if (ht.containsKey("Animals"))
			;
		System.out.println("Animals was found");
		// Tìm ra
		Integer year = (Integer) ht.get("Wish You Were Here");
		System.out.println("Wish you Were Here was released in" + year.toString());
		// Xoá một tập ảnh
		System.out.println("Removing Ummagumma\r\n");
		ht.remove("Ummagumma");
		// Di chuyển thông qua một bảng liệt kê của tất cả các khoá trong bảng.
		System.out.println("Remaining:\r\n");
		for (Enumeration enumer = ht.keys(); enumer.hasMoreElements();)
			System.out.println((String) enumer.nextElement());
	}
}
