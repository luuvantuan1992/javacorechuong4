package Example2;

import java.util.*;
import java.util.Vector;

public class VectorImplementation {
	public static void main(String args[]) {
		Vector vect = new Vector();
		vect.addElement("One");
		vect.addElement("Two");
		vect.addElement("Three");
		vect.addElement("Four");
		vect.addElement("Five");
		vect.insertElementAt("Numbers In Words", 0);
		vect.insertElementAt("Four", 4);
		System.out.println("Size: " + vect.size());
		System.out.println("Vector ");
		for (int i = 0; i < vect.size(); i++) {
			System.out.println(vect.elementAt(i) + " , ");
		}
		vect.removeElement("Five");
		System.out.println("");
		System.out.println("Size: " + vect.size());
		System.out.println("Vector ");
		for (int i = 0; i < vect.size(); i++) {
			System.out.print(vect.elementAt(i) + " , ");
		}
	}
}
