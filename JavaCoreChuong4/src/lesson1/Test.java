package lesson1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Test implements MyPower {

	@Override
	public double getSquaredValue(double value) {
		return Math.pow(value, 2);
	}

	@Override
	public double getCubeValue(double value) {
		return Math.pow(value, 3);
	}

	public static void main(String[] args) {
		Test test = new Test();
		System.out.println("Nhập vào giá trị cần tính bình phương ...");
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		try {
			double value = Double.parseDouble(br.readLine());
			System.out.println("Giá trị bình phương của " + value + " là " + test.getSquaredValue(value));
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		System.out.println("Nhập vào giá trị cần tính lập phương...");
		try {
			double value = Double.parseDouble(br.readLine());
			System.out.println("Giá trị lập phương của " + value + " là " + test.getCubeValue(value));
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
