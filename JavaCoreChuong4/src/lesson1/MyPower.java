package lesson1;

public interface MyPower {

	public double getSquaredValue(double value);

	public double getCubeValue(double value);
}
