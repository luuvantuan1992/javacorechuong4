package lesson5;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class MyStringTokenizer {

	public static void main(String[] args) {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Nhập vào số điện thoại");
		try {
			String str = br.readLine();
			StringTokenizer stz = new StringTokenizer(str);
			do {
				System.out.println("Mã quốc gia là " + stz.nextToken());
			} while (false);
			String arr = stz.nextToken();
			String[] arr2 = arr.split("\\-");
			System.out.println("Mã vùng là " + arr2[0]);

			System.out.println("Số điện thoại là " + arr2[1]);

		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
