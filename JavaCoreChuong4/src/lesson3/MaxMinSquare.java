package lesson3;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.SortedSet;
import java.util.TreeSet;

public class MaxMinSquare {

	private static int number;

	public static void main(String[] args) {
		System.out.println("Nhập vào dãy các số...");
		MaxMinSquare mms = new MaxMinSquare();
		SortedSet<Double> t = mms.dataEntry(new TreeSet<Double>());

		System.out.println("Số nhỏ nhất là " + t.first().toString()
				+ " ,giá trị bình phương của nó là " + Math.pow(t.first(), 2));
		System.out.println("Số lớn nhất là " + t.last().toString()
				+ " ,giá trị bình phương của nó là " + Math.pow(t.last(), 2));
	}

	public SortedSet<Double> dataEntry(SortedSet<Double> sortedSet) {
		String str;
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Số phần tử cần nhập vào để tính toán là...");
		try {
			str = br.readLine();
			number = Integer.parseInt(str);
		} catch (NumberFormatException e) {
			System.out.println("Kí tự nhập vào không phải là 1 số");
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		for (int i = 0; i < number; i++) {

			System.out.println("Nhập vào số thứ " + (i + 1));
			try {
				str = br.readLine();
				Double value = Double.parseDouble(str);
				sortedSet.add(value);
			} catch (NumberFormatException e) {
				System.out
						.println("Bạn vửa nhập vào không thỏa mãn là 1 số...");
			} catch (IOException e) {
				e.printStackTrace();
			}

		}
		return sortedSet;
	}

}

